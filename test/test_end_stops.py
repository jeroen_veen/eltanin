import io
import serial
import time
from subprocess import Popen
from src.rpi.checkOS import is_raspberry_pi
import traceback
import asyncio

port = '/tmp/printer'

async def reply(sio):
    while True:
        reply_msg = sio.readline()
        if reply_msg:
            print("info; printHat replied: " + reply_msg)
            break
    return reply_msg


async def send_gcode(sio, gcode_string):
    if sio:
        try:
            sio.write(gcode_string + "\r\n")
            sio.flush()  # it is buffering. required to get the data out *now*
            print("info; send gcode: " + gcode_string)

            await asyncio.sleep(.5)
            await reply(sio)

        except Exception as err:
            traceback.print_exc()
    else:
        print("error;no serial connection with printHat.")


if not is_raspberry_pi():
    print("This function is for Raspberypi with printhat!")
    exit(-1)

ps = Popen(['sudo', 'service', 'klipper', 'restart'])
# out = check_output(['sudo', 'service', 'klipper', 'status'])
# print('\n', out, '\n')
time.sleep(1)

ser = serial.Serial(port, 250000, timeout=1)
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser), line_buffering=True, encoding="utf-8", errors='replace')  # , newline="\r\n")

if sio:
    print("info; connected to printHat via serial port {}".format(port))
else:
    print("error; cannot connect to printHat via serial port {}".format(port))

asyncio.run(send_gcode(sio, "RESTART"))



asyncio.run(send_gcode(sio, "M115"))  # get firmware details
asyncio.run(send_gcode(sio, "QUERY_ENDSTOPS"))
# asyncio.run(send_gcode(sio, "G28 X Y Z"))  # move to origin
# asyncio.run(send_gcode(sio, "M114"))  # get current position
#
# asyncio.run(send_gcode(sio, "G1 X100 Y100 Z10"))  # move to position

val = 0.5
asyncio.run(send_gcode(sio, "SET_PIN PIN=rpi_fan VALUE={:1.2f}".format(val)))

while True:
    pass

# send_gcode(sio, 'M112')
ser.close()
ps = Popen(['sudo', 'service', 'klipper', 'stop'])
