"""
@package BatchProcessor.py
@brief batch well positioning.
@author Gert van Lagen, Robin Meekes, Jeroen Veen
"""
from os import path, sep, getcwd, remove, mkdir
from shutil import copy
from glob import glob
from time import strptime, time
import re
from wait import wait_ms, wait_signal
from PyQt5.QtCore import QSettings, QObject, pyqtSignal, pyqtSlot, QTimer
from PyQt5.QtWidgets import QDialog, QFileDialog, QPushButton, QLabel, QSpinBox, QDoubleSpinBox, QVBoxLayout, \
    QGridLayout
from subprocess import run
import smtplib, ssl


class Well(object):
    def __init__(self, name: str, position: str, location: list, note: str = None):
        self.name = name
        self.position = position
        self.location = location
        self.note = note


class WellPlate(object):
    def __init__(self, nr_of_columns: int, column_well_spacing: float,
                 nr_of_rows: int, row_well_spacing: float, wells: list = [],
                 offset: list = [], name: str = None, id_str: str = None, note: str = None):
        self.name = name
        self.id_str = id_str
        self.note = note
        self.nr_of_columns = nr_of_columns
        self.column_well_spacing = column_well_spacing
        self.nr_of_rows = nr_of_rows
        self.row_well_spacing = row_well_spacing
        self.offset = offset
        self.wells = wells


class Batch(object):
    def __init__(self, id_str, duration, interleave, note=None, shutdown=False, snapshot=False, autofocus=False,
                 video_clip_duration=0):
        self.id_str = id_str
        self.duration = duration
        self.interleave = interleave
        self.note = note
        self.shutdown = shutdown
        self.snapshot = snapshot
        self.autofocus = autofocus
        self.video_clip_duration = video_clip_duration


def load_well_plate(batch_settings: QSettings = None):
    """ Load wells from batch settings and compute physical locations
        :param batch_settings: QSettings object
        :return: well list
    """
    note = batch_settings.value('plate/note')
    nr_of_columns = batch_settings.value('plate/nr_of_columns', 0, type=int)
    nr_of_rows = batch_settings.value('plate/nr_of_rows', 0, type=int)
    A1_to_side_offset = batch_settings.value('plate/A1_to_side_offset', 0, type=float)
    column_well_spacing = batch_settings.value('plate/column_well_spacing', 0, type=float)
    A1_to_top_offset = batch_settings.value('plate/A1_to_top_offset', 0, type=float)
    row_well_spacing = batch_settings.value('plate/row_well_spacing', 0, type=float)

    # compute well locations
    nr_of_wells = batch_settings.beginReadArray("wells")
    wells = []
    for i in range(0, nr_of_wells):
        batch_settings.setArrayIndex(i)
        well_id = batch_settings.value('id')
        well_note = batch_settings.value('note')
        r = re.split('(\d+)', well_id)
        row = ord(r[0].lower()) - 96
        col = int(r[1])
        location_mm = [round(A1_to_side_offset + (col - 1) * column_well_spacing, 2), \
                       round(A1_to_top_offset + (row - 1) * row_well_spacing, 2), \
                       0]
        wells.append(Well(name=well_id, position=[row, col], note=well_note, location=location_mm))
    batch_settings.endArray()  # close array, also required when opening!

    return WellPlate(note=note, nr_of_columns=nr_of_columns, column_well_spacing=column_well_spacing,
                     nr_of_rows=nr_of_rows, row_well_spacing=row_well_spacing, wells=wells,
                     offset=[A1_to_side_offset, A1_to_top_offset])


def load_batch_run_settings(batch_settings: QSettings = None):
    """ Load batch run info into class variables
        :param batch_settings: QSettings object
        :return: batch object
    """
    id_str = batch_settings.value('run/id')
    note = batch_settings.value('run/note')
    t = strptime(batch_settings.value('run/duration')[1], '%H:%M:%S')
    days = int(batch_settings.value('run/duration')[0].split('d')[0])
    duration = ((24 * days + t.tm_hour) * 60 + t.tm_min) * 60 + t.tm_sec
    t = strptime(batch_settings.value('run/wait'), '%H:%M:%S')
    run_wait = (t.tm_hour * 60 + t.tm_min) * 60 + t.tm_sec
    shutdown = batch_settings.value('run/shutdown', False, type=bool)
    snapshot = batch_settings.value('run/snapshot', False, type=bool)
    autofocus = batch_settings.value('run/autofocus', False, type=bool)
    video_clip = batch_settings.value('run/videoclip', True, type=bool)
    video_clip_duration = batch_settings.value('run/clip_length', 0, type=int)
    video_clip_duration = video_clip_duration if video_clip else 0

    return Batch(id_str, duration, run_wait, note, shutdown, snapshot, autofocus, video_clip_duration)


class ManualLocationingDialog(QDialog):

    def __init__(self, location):
        super(ManualLocationingDialog, self).__init__()

        self.setWindowTitle("Manual positioning")
        self.instruction = QLabel("Please focus on well A1, adjust illumination intensity and press OK when done")

        self.light = QSpinBox(self)
        self.lightTitle = QLabel("light")
        self.light.setSuffix("%")
        self.light.setMinimum(0)
        self.light.setMaximum(100)
        self.light.setSingleStep(1)
        self.stageXTranslation = QDoubleSpinBox(self)
        self.stageXTranslationTitle = QLabel("X Translation")
        self.stageXTranslation.setSuffix("mm")
        self.stageXTranslation.setMinimum(0.0)
        self.stageXTranslation.setMaximum(100)
        self.stageXTranslation.setSingleStep(0.1)
        self.stageXTranslation.setValue(location[0])
        self.stageYTranslation = QDoubleSpinBox(self)
        self.stageYTranslationTitle = QLabel("Y Translation")
        self.stageYTranslation.setSuffix("mm")
        self.stageYTranslation.setMinimum(0.0)
        self.stageYTranslation.setMaximum(100)
        self.stageYTranslation.setSingleStep(0.1)
        self.stageYTranslation.setValue(location[1])
        self.stageZTranslation = QDoubleSpinBox(self)
        self.stageZTranslationTitle = QLabel("Z Translation")
        self.stageZTranslation.setSuffix("mm")
        self.stageZTranslation.setMinimum(0.0)
        self.stageZTranslation.setMaximum(50)
        self.stageZTranslation.setSingleStep(0.01)
        self.stageZTranslation.setValue(location[2])

        self.okButton = QPushButton("OK")
        self.okButton.clicked.connect(self.okclicked)

        layout = QVBoxLayout()
        layout.addWidget(self.instruction)
        layout2 = QGridLayout()
        layout2.addWidget(self.stageXTranslationTitle, 0, 0)
        layout2.addWidget(self.stageXTranslation, 0, 1)
        layout2.addWidget(self.stageYTranslationTitle, 1, 0)
        layout2.addWidget(self.stageYTranslation, 1, 1)
        layout2.addWidget(self.stageZTranslationTitle, 2, 0)
        layout2.addWidget(self.stageZTranslation, 2, 1)
        layout.addLayout(layout2)
        layout.addWidget(self.lightTitle)
        layout.addWidget(self.light)
        layout.addWidget(self.okButton)
        self.setLayout(layout)

    @pyqtSlot()
    def okclicked(self):
        self.accept()


class BatchProcessor(QObject):
    post_message = pyqtSignal(str)
    goto_location = pyqtSignal(list, bool)  # boolean indicate relative to stage origin or not
    go_home = pyqtSignal()
    set_light_PWM = pyqtSignal(float)  # control signal
    start_camera = pyqtSignal()
    stop_camera = pyqtSignal()
    take_snapshot = pyqtSignal(str)
    record_clip = pyqtSignal(str, int)
    r_snapshot_taken = pyqtSignal()  # repeater signal
    r_clip_recorded = pyqtSignal()  # repeat signal
    r_confirmed = pyqtSignal()  # repeat signal
    progress = pyqtSignal(int)
    done = pyqtSignal()
    closed = pyqtSignal()
    # findDiaphragm = pyqtSignal()
    # findWell = pyqtSignal()
    #     computeSharpnessScore = pyqtSignal()
    #     rSharpnessScore = pyqtSignal() # repeater signal
    # rWellFound = pyqtSignal()  # repeat signal
    # rDiaphragmFound = pyqtSignal()  # repeat signal
    # rlocationReached = pyqtSignal()  # repeat signal
    # setLogFileName = pyqtSignal(str)
    # startAutoFocus = pyqtSignal(float)
    # focussed = pyqtSignal()  # repeater signal

    timer = QTimer()

    def __init__(self, settings: QSettings = None):
        super().__init__()
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings

        self.resolution = float(self.settings.value('camera/resolution_in_px_per_mm'))
        self.storage_path = sep.join([getcwd(), self.settings.value('temp_folder', None, type=str)])
        self.image_storage_path = sep.join([self.storage_path, 'img'])
        self.server_storage_path, self.subscriber = None, None
        connection_settings_filename = sep.join([path.dirname(self.settings.fileName()),
                                                 self.settings.value('connection_settings')])
        print(f"{self.__class__.__name__}: info; loading connection settings from {connection_settings_filename}")
        self.conn_settings = QSettings(connection_settings_filename, QSettings.IniFormat)
        self.batch_run = None
        self.well_plate = None
        self.prev_note_nr = 0  # for logging
        self.light_Level = 1.0  # initial value, will be set later by user
        self.timer.timeout.connect(self.timer_callback)
        if not path.exists(self.image_storage_path):
            # create temporary image storage path
            mkdir(self.image_storage_path)
        print(f"{self.__class__.__name__}: info; initialized")

    @pyqtSlot()
    def stop(self):
        """ Stop batch processing
            :return: None
        """
        self.post_message.emit(f"{self.__class__.__name__}: info; stopping")
        if self.timer.isActive():
            self.timer.stop()

    @pyqtSlot()
    def start(self):
        """ Start batch processing
            :return: None
        """
        # clear temporary storage path
        files = glob(sep.join([self.storage_path, '*.*']))
        for file in files:
            remove(file)

        # open batch definition file
        dlg = QFileDialog()
        batch_file_name = QFileDialog.getOpenFileName(dlg, 'Open batch definition file',
                                                      path.dirname(self.settings.fileName()), "Ini file (*.ini)")[0]
        if batch_file_name == "" or batch_file_name is None:
            self.post_message.emit(f"{self.__class__.__name__}: "
                                  f"error; no batch settings loaded")
            return
        batch_settings = QSettings(batch_file_name, QSettings.IniFormat)
        if not ('batch' in batch_settings.value('filetype', None, type=str)):
            self.post_message.emit(f"{self.__class__.__name__}: "
                                  f"error; this is not a batch settings file")
            return
        self.load_batch_settings(batch_settings)
        copy(batch_file_name, self.storage_path)
        # run(['cp', batch_file_name, self.storage_path])

        # setup directory remote structure
        self.server_storage_path, self.subscriber = self.setup_connectivity(self.conn_settings, batch_settings)
        if self.server_storage_path is not None:
            try:
                run(['rclone', 'mkdir', self.server_storage_path])
                run(['rclone', 'copy', '--no-traverse', self.storage_path, self.server_storage_path])
                # create directory structure on server
                for well in self.well_plate.wells:
                    run(['rclone', 'mkdir', path.sep.join([self.server_storage_path, well.name])])
            except Exception as err:
                self.post_message.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

        # The user may change the location of well A1 and set both z-axis and illumination
        location, self.light_Level = self.get_location_from_user(self.well_plate.wells[0].location)
        offset = [self.well_plate.wells[0].location[0] - location[0],
                  self.well_plate.wells[0].location[1] - location[1]]
        for well in self.well_plate.wells:
            well.location[0] -= offset[0]
            well.location[1] -= offset[1]
            well.location[2] = location[2]  # copy z, later on we may do autofocus per well

        # start timer
        self.prev_note_nr = 0  # for logging
        self.batch_run.start_time = time()
        self.timer.start(0)

    def timer_callback(self):
        """ Timer call back function, als initiates next one-shot
        """
        start_run_time = time()
        self.set_light_PWM.emit(self.light_Level)
        self.start_camera.emit()
        self.goto_location.emit(self.well_plate.wells[0].location, True)
        wait_signal(self.r_confirmed, 30000)
        for well in self.well_plate.wells:
            self.post_message.emit(f"{self.__class__.__name__}: info; gauging well {well.name}")
            self.goto_location.emit(well.location, True)
            if wait_signal(self.r_confirmed, 100000) < 0:
                self.post_message.emit(f"{self.__class__.__name__}: error; time out, location not reached.")

            # todo: autofocus
            if self.batch_run.autofocus:
                self.post_message.emit(f"{self.__class__.__name__}: info; starting autofocus")
                pass

            # clear temporary storage path
            try:
                # clear temporary storage path
                files = glob(sep.join([self.image_storage_path, '*.*']))
                for file in files:
                    remove(file)
            except Exception as err:
                self.post_message.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

            # take snapshot or video
            wait_ms(3000)
            prefix = sep.join([self.image_storage_path, str(well.location) + "_" + str(well.location)])
            if self.batch_run.snapshot:
                self.take_snapshot.emit(prefix)
                wait_signal(self.r_snapshot_taken, 30000)  # snapshot taken
            if self.batch_run.video_clip_duration > 0:
                self.record_clip.emit(prefix, self.batch_run.video_clip_duration)
                wait_signal(self.r_clip_recorded, 1000*(self.batch_run.video_clip_duration+30))  # clip recorded

            # push capture to remote
            if (self.batch_run.snapshot or self.batch_run.video_clip_duration > 0) \
                    and self.server_storage_path is not None:
                try:
                    temp_path = sep.join([self.server_storage_path, well.name])
                    run(['rclone', 'copy', '--no-traverse', self.image_storage_path, temp_path])
                except Exception as err:
                    self.post_message.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

        # wrap up current round of acquisition
        elapsed_total_time = time() - self.batch_run.start_time
        elapsed_run_time = time() - start_run_time
        remaining_time = int(self.batch_run.duration - elapsed_total_time)
        self.post_message.emit(f"{self.__class__.__name__}: info; single run time={elapsed_run_time:.1f}s")
        self.post_message.emit(f"{self.__class__.__name__}: info; total run time={elapsed_total_time:.1f}s")

        # check if we still have time to do another round
        if elapsed_total_time + self.batch_run.interleave < self.batch_run.duration:
            progress_percentage = int(100 * elapsed_total_time / self.batch_run.duration)
            self.timer.setInterval(self.batch_run.interleave * 1000)
            self.post_message.emit(f"{self.__class__.__name__}: info; wait for {self.batch_run.interleave:.1f} s")
        else:
            progress_percentage = 100
            self.timer.stop()
            self.done.emit()
            self.post_message.emit(f"{self.__class__.__name__}: info; run finalized")

        # Notify user on progress
        self.progress.emit(progress_percentage)
        self.post_message.emit(f"{self.__class__.__name__}: info; progress: {progress_percentage}%")
        note_nr = int(progress_percentage / 10)
        if note_nr > self.prev_note_nr:
            self.prev_note_nr = note_nr
            message = """Subject: run finalized""" if progress_percentage == 100 else \
                f"""Subject: Progress = {progress_percentage}% \n\n Still {remaining_time} s left"""
            # do some fancy message in future: https://realpython.com/python-send-email/#sending-fancy-emails
            self.send_notification(message)

        # push log file to server
        run(['rclone', 'copy', '--no-traverse', self.storage_path, self.server_storage_path])

        # Shut off
        self.stop_camera.emit()
        self.set_light_PWM.emit(0.00)
        # self.go_home.emit()
        if progress_percentage == 100 and self.batch_run.shutdown:
            self.go_home.emit()
            self.post_message.emit(f"{self.__class__.__name__}: info; shut down")
            self.closed.emit()

    def get_location_from_user(self, start_location):
        """ Let user move translational stage and return final location
            :param start_location: initial location
            :return: final location and illumination level
        """
        self.start_camera.emit()
        self.set_light_PWM.emit(0.2)

        self.goto_location.emit(start_location, True)

        # Let the user set the x,y,z-location manually by moving to first well and open dialog
        dialog = ManualLocationingDialog(start_location)
        dialog.stageXTranslation.valueChanged.connect(lambda val: self.goto_location.emit([val, None, None], True))
        dialog.stageYTranslation.valueChanged.connect(lambda val: self.goto_location.emit([None, val, None], True))
        dialog.stageZTranslation.valueChanged.connect(lambda val: self.goto_location.emit([None, None, val], True))
        dialog.light.valueChanged.connect(lambda val: self.set_light_PWM.emit(val / 100))
        dialog.exec_()

        x = round(dialog.stageXTranslation.value(), 3)
        y = round(dialog.stageYTranslation.value(), 3)
        z = round(dialog.stageZTranslation.value(), 3)
        light = round(dialog.light.value() / 100, 3)

        return [x, y, z], light

    def load_batch_settings(self, batch_settings: QSettings = None):
        """ Load batch processing into class variables
            :param batch_settings: QSettings object
            :return: None
        """
        self.post_message.emit(f"{self.__class__.__name__}: "
                              f"info; loading batch settings from {batch_settings.fileName()}")
        self.batch_run = load_batch_run_settings(batch_settings)
        self.well_plate = load_well_plate(batch_settings)

        self.post_message.emit(f"{self.__class__.__name__}: "
                              f"info; run note: {self.batch_run.note}")
        self.post_message.emit(f"{self.__class__.__name__}: "
                              f"info; plate note: {self.well_plate.note}")
        self.post_message.emit(f"{self.__class__.__name__}: "
                              f"info; {len(self.well_plate.wells)} wells found in {batch_settings.fileName()}")
        self.post_message.emit(f"{self.__class__.__name__}: "
                              f"info; {self.batch_run.duration}s run with {self.batch_run.interleave}s interleave")

    def setup_connectivity(self, conn_settings: QSettings = None, batch_settings: QSettings = None):
        """ Set-up connectivity
            :param conn_settings: QSettings object
            :param batch_settings: QSettings object
            :return: server_storage_path
        """
        subscriber = batch_settings.value('connections/email', None, type=str)
        server_storage_path = None
        if 'rclone' in batch_settings.value('connections/storage', None, type=str):
            # rclone to path provided in connections.ini file
            server_storage_path = conn_settings.value('rclone/storage_path') + ':' + batch_settings.value('run/id')
            self.post_message.emit(f"{self.__class__.__name__}: info; rclone connection {server_storage_path}")
        else:
            self.post_message.emit(f"{self.__class__.__name__}: error; unknown remote")

        return server_storage_path, subscriber

    def send_notification(self, message: str):
        """ Send notification, currently via email
            :param message: message to send (both subject and body)
            :return: None
        """
        self.post_message.emit(f"{self.__class__.__name__}: info; sending notification: {message} to {self.subscriber}")
        port = 465  # For SSL
        context = ssl.create_default_context()  # Create a secure SSL context
        try:
            with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
                login = self.conn_settings.value('smtp/login')
                password = self.conn_settings.value('smtp/password')
                server.login(login, password)
                server.sendmail(self.conn_settings.value('smtp/login'), self.subscriber, message)
        except Exception as err:
            self.post_message.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def snapshot_taken(self):
        """ Repeater signal
        """
        self.r_snapshot_taken.emit()

    @pyqtSlot()
    def clip_recorded(self):
        """ Repeater signal
        """
        self.r_clip_recorded.emit()

    @pyqtSlot()
    def location_confirmed(self):
        """ Repeater signal
        """
        self.post_message.emit(f"{self.__class__.__name__}: info; confirmed signal received.")
        self.r_confirmed.emit()