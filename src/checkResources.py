from PyQt5.QtCore import QTimer, QObject, pyqtSignal, pyqtSlot
import psutil


class CheckResources(QObject):
    timer = QTimer()
    postMessage = pyqtSignal(str)

    def __init__(self, interval=100):
        super().__init__()

        self.interval = 1000 * interval
        self.timer.timeout.connect(self.update)
        self.timer.start(self.interval)

    def update(self):
        try:
            cpu = psutil.cpu_percent()
            memory = psutil.virtual_memory()
            available = round(memory.available / 1024.0 / 1024.0, 1)
            total = round(memory.total / 1024.0 / 1024.0, 1)
            self.postMessage.emit(f"{self.__class__.__name__}: info; "
                                  f"CPU load={cpu:.1f}%, RAM usage={total - available :.1f} MB of {total :.1f} MB")
            # m = psutil.Process().memory_info().rss
            # self.postMessage.emit(f"{self.__class__.__name__}: info; "
            #                       f"Resident Set Size = {psutil._common.bytes2human(m)}")

        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def stop(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopping")
        if self.timer.isActive():
            self.timer.stop()


if __name__ == "__main__":
    from PyQt5.QtCore import QCoreApplication
    from testMessageSink import TestMessageSink

    app = QCoreApplication([])

    cr = CheckResources(20)
    tm = TestMessageSink()

    cr.postMessage.connect(tm.receive_message)

    app.exec_()
