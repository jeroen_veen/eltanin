from os import sep, remove
from glob import glob
import sys

sys.path.append("rpi")

from PyQt5.QtCore import Qt, QSettings
from PyQt5.QtWidgets import QApplication

from rpi.checkConnection import CheckInternet
from rpi.checkOS import is_raspberry_pi
from rpi.checkCamera import check_camera
from rpi.checkTemperature import CheckTemperature
from checkResources import CheckResources
from rpi.printHat import PrintHat
from mainWindow import MainWindow
from rpi.PyQtPiCam import PiVideoStream
from batchProcessor import BatchProcessor
from log import LogWindow
from wait import wait_ms
import qdarkstyle


CFG_PATH = 'cfg'

if __name__ == '__main__':
    if not is_raspberry_pi():
        print("ERROR: This app is for raspberrypi")
        exit()
#    if not check_camera():
#        print("ERROR: No supported camera detected")
#        exit()

    settings_filename = sep.join([CFG_PATH, 'settings.ini'])
    settings = QSettings(settings_filename, QSettings.IniFormat)

    files = glob(sep.join([settings.value('temp_folder', None, type=str), '*.*']))
    for file in files:
        remove(file)

    # Create event loop and instantiate objects
    app = QApplication(sys.argv)
    dark_stylesheet = qdarkstyle.load_stylesheet_pyqt5()
    app.setStyleSheet(dark_stylesheet)

    mw = MainWindow(settings)
    lw = LogWindow(settings)
    vs = PiVideoStream(settings)
    ph = PrintHat(settings)
    st = CheckTemperature(interval=10, alarm_temperature=55)
    bp = BatchProcessor(settings)
    # ip = ImageProcessor()
    # af = AutoFocus(display=True)
    cf = CheckInternet()
    cr = CheckResources(600)

    # Connect logging signals
    # bp.setLogFileName.connect(lw.setLogFileName)
    bp.post_message.connect(lw.append)
    vs.postMessage.connect(lw.append)
    ph.postMessage.connect(lw.append)
    st.postMessage.connect(lw.append)
    cf.postMessage.connect(lw.append)
    cr.postMessage.connect(lw.append)

    # Connect functional signals
    st.alarm.connect(lambda: ph.set_fan_PWM(50))  # min(abs(st.temperature - st.threshold) / 10, 10)))
    st.alarmRemoved.connect(lambda: ph.set_fan_PWM(0))
    vs.frame.connect(mw.update, type=Qt.BlockingQueuedConnection)
    mw.light.valueChanged.connect(lambda x: ph.set_light_PWM(x / 100))
    mw.snapshotButton.clicked.connect(lambda: vs.take_image())
    mw.recordButton.clicked.connect(lambda: vs.record_clip())
    mw.runButton.clicked.connect(bp.start)
    mw.runButton.released.connect(mw.disableButtons)
    mw.loadButton.clicked.connect(ph.go_to_load_location)
    bp.goto_location.connect(ph.goto_location)
    bp.go_home.connect(ph.go_home)
    bp.progress.connect(mw.updateProgressBar)
    bp.done.connect(mw.enableButtons)
    bp.set_light_PWM.connect(ph.set_light_PWM)
    # bp.disableMotors.connect(ph.disableMotors)
    # bp.gotoXY.connect(ph.gotoXY, type=Qt.QueuedConnection)
    # bp.gotoX.connect(ph.gotoX, type=Qt.QueuedConnection)
    # bp.gotoY.connect(ph.gotoY, type=Qt.QueuedConnection)
    # bp.gotoZ.connect(ph.gotoZ, type=Qt.QueuedConnection)
    # ph.locationReached.connect(bp.locationReached, type=Qt.QueuedConnection)
    # bp.findDiaphragm.connect(ip.findDiaphragm, type=Qt.QueuedConnection)
    # ip.diaphragmFound.connect(bp.diaphragmFound, type=Qt.QueuedConnection)
    # ip.quality.connect(mw.imageQualityUpdate)
    # bp.findWell.connect(ip.findWell, type=Qt.QueuedConnection)
    # ip.wellFound.connect(bp.wellFound, type=Qt.QueuedConnection)
    bp.take_snapshot.connect(vs.take_image, type=Qt.QueuedConnection)
    vs.captured.connect(bp.snapshot_taken, type=Qt.QueuedConnection)
    bp.record_clip.connect(vs.record_clip, type=Qt.QueuedConnection)
    vs.captured.connect(bp.clip_recorded, type=Qt.QueuedConnection)
    ph.confirmed.connect(bp.location_confirmed, type=Qt.QueuedConnection)
    # bp.startAutoFocus.connect(af.start)
    # af.focussed.connect(bp.focussedSlot)
    bp.stop_camera.connect(vs.stop, type=Qt.QueuedConnection)
    bp.start_camera.connect(vs.init_stream, type=Qt.QueuedConnection)
    mw.breakButton.clicked.connect(ph.emergency_break)
    mw.restartButton.clicked.connect(ph.firmware_restart)
    mw.homeButton.clicked.connect(ph.go_home)
    mw.stageOriginButton.clicked.connect(lambda: ph.goto_location(3 * [0]))
    mw.stageXTranslation.valueChanged.connect(lambda x: ph.goto_location([x, None, None]))
    mw.stageYTranslation.valueChanged.connect(lambda y: ph.goto_location([None, y, None]))
    mw.stageZTranslation.valueChanged.connect(lambda z: ph.goto_location([None, None, z]))

    # Connect closing signals
    mw.closed.connect(lw.close, type=Qt.QueuedConnection)
    st.failure.connect(lw.close, type=Qt.QueuedConnection)
    lw.closed.connect(ph.stop, type=Qt.QueuedConnection)
    lw.closed.connect(st.stop, type=Qt.QueuedConnection)
    lw.closed.connect(cf.stop, type=Qt.QueuedConnection)
    lw.closed.connect(vs.stop, type=Qt.QueuedConnection)
    lw.closed.connect(bp.stop, type=Qt.QueuedConnection)
    lw.closed.connect(mw.close, type=Qt.QueuedConnection)
    bp.closed.connect(mw.close, type=Qt.QueuedConnection)

    # Start the show
    mw.move(100, 100)
    mw.resize(1500, 500)
    lw.move(100, 800)
    lw.resize(1500, 200)
    mw.show()
    lw.show()

    # Start-up recipe
    mw.disableButtons()
    ph.connect_klipper()
    wait_ms(500)
    ph.go_home()
    mw.enableButtons()
    vs.init_stream()

    app.exec_()
