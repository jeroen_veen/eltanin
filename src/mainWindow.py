"""@package docstring
MainWindow
"""
import cv2
from time import time
import numpy as np
import matplotlib
from PyQt5.QtGui import QCloseEvent, QImage, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QTimer, QEventLoop, QSettings
from PyQt5.QtWidgets import QMainWindow, QWidget, QDesktopWidget, QLabel, QPushButton, QProgressBar, QSpinBox, \
    QDoubleSpinBox, QGridLayout, QHBoxLayout, QSpacerItem, QSizePolicy

# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt


class MainWindow(QMainWindow):
    postMessage = pyqtSignal(str)
    closed = pyqtSignal()

    settings: QSettings
    image: np.array

    def __init__(self, settings=None):
        super(MainWindow, self).__init__()

        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings

        frame_size_str = self.settings.value('gui_frame_size')
        (width, height) = frame_size_str.split('x')
        self.image_size = (int(width), int(height))

        self.central_widget = QWidget()  # define central widget
        self.setCentralWidget(self.central_widget)  # set QMainWindow.centralWidget

        # self.image = None
        self.prev_clock_time = None

        self.setWindowTitle(self.settings.value('name'))
        screen = QDesktopWidget().availableGeometry()
        self.image_width = round(screen.height() * 0.8)
        self.image_height = round(screen.width() * 0.8)
        self.image_scaling_factor = 1.0
        self.image_scaling_step = 0.1

        # Labels
        self.PixImage = QLabel()
        self.timerLabel = QLabel()
        self.imageQualityLabel = QLabel()
        self.temperatureLabel = QLabel()

        # a figure instance to plot on
        self.canvas = FigureCanvas(Figure())  # (figsize=(5, 3)))
        self.axes = self.canvas.figure.subplots(2, 2, sharex=False, sharey=False)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        ##        self.toolbar = NavigationToolbar(self.canvas, self)

        # Buttons
        self.breakButton = QPushButton("Emergency break")
        self.restartButton = QPushButton("Restart firmware")
        self.homeButton = QPushButton("Home")
        self.snapshotButton = QPushButton("Snapshot")
        self.autoFocusButton = QPushButton("AutoFocus")
        self.recordButton = QPushButton("Record")
        self.stageOriginButton = QPushButton("Stage Origin")
        self.runButton = QPushButton("Run")
        self.loadButton = QPushButton("Load")

        # Progress bar
        self.progress_bar = QProgressBar(self)
        self.progress_bar.setValue(0)

        # Spinboxes
        self.light = QSpinBox(self)
        self.lightTitle = QLabel("light")
        self.light.setSuffix("%")
        self.light.setMinimum(0)
        self.light.setMaximum(100)
        self.light.setSingleStep(1)
        self.stageXTranslation = QDoubleSpinBox(self)
        self.stageXTranslationTitle = QLabel("X Translation")
        self.stageXTranslation.setSuffix("mm")
        self.stageXTranslation.setMinimum(0.0)
        self.stageXTranslation.setMaximum(100)
        self.stageXTranslation.setSingleStep(0.1)
        self.stageYTranslation = QDoubleSpinBox(self)
        self.stageYTranslationTitle = QLabel("Y Translation")
        self.stageYTranslation.setSuffix("mm")
        self.stageYTranslation.setMinimum(0.0)
        self.stageYTranslation.setMaximum(100)
        self.stageYTranslation.setSingleStep(0.1)
        self.stageZTranslation = QDoubleSpinBox(self)
        self.stageZTranslationTitle = QLabel("Z Translation")
        self.stageZTranslation.setSuffix("mm")
        self.stageZTranslation.setMinimum(0.0)
        self.stageZTranslation.setMaximum(50)
        self.stageZTranslation.setSingleStep(0.01)
        self.VCSpinBox = QDoubleSpinBox(self)
        self.VCSpinBoxTitle = QLabel("VC")
        self.VCSpinBox.setSuffix("%")
        self.VCSpinBox.setMinimum(-100.0)
        self.VCSpinBox.setMaximum(100.0)
        self.VCSpinBox.setSingleStep(0.1)

        # Compose layout grid
        self.keyWidgets = [self.stageXTranslationTitle, self.stageYTranslationTitle, self.stageZTranslationTitle,
                           self.lightTitle, self.VCSpinBoxTitle]
        self.valueWidgets = [self.stageXTranslation, self.stageYTranslation, self.stageZTranslation, self.light,
                             self.VCSpinBox]

        widget_layout = QGridLayout()
        for index, widget in enumerate(self.keyWidgets):
            if widget is not None:
                widget_layout.addWidget(widget, index, 0, Qt.AlignCenter)
        for index, widget in enumerate(self.valueWidgets):
            if widget is not None:
                widget_layout.addWidget(widget, index, 1, Qt.AlignCenter)

        widget_layout.addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))  # variable space
        widget_layout.addWidget(self.loadButton, index + 1, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.runButton, index + 2, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.progress_bar, index + 2, 1, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.breakButton, index + 3, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.restartButton, index + 3, 1, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.homeButton, index + 4, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.stageOriginButton, index + 4, 1, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.autoFocusButton, index + 5, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.snapshotButton, index + 6, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.recordButton, index + 6, 1, alignment=Qt.AlignLeft)
#         widget_layout.addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))  # variable space
        widget_layout.addWidget(QLabel("Processing time [ms]: "), index + 8, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.timerLabel, index + 8, 1, alignment=Qt.AlignLeft)
        widget_layout.addWidget(QLabel("Image quality [au]: "), index + 9, 0, alignment=Qt.AlignLeft)
        widget_layout.addWidget(self.imageQualityLabel, index + 9, 1, alignment=Qt.AlignLeft)
        ##        widgetLayout.addWidget(QLabel("Temperature [°C]: "),index+8,0,alignment=Qt.AlignLeft)
        ##        widgetLayout.addWidget(self.temperatureLabel,index+8,1,alignment=Qt.AlignLeft)

        # Set specific gui element values
        for index, widget in enumerate(self.keyWidgets):  # retreive all labeled parameters
            if isinstance(widget, QLabel):
                key = "mainwindow/" + widget.text()
                if self.settings.contains(key):
                    self.valueWidgets[index].setValue(float(self.settings.value(key)))

        # Compose final layout
        layout = QHBoxLayout()
        layout.addLayout(widget_layout, Qt.AlignTop | Qt.AlignCenter)
        layout.addWidget(self.PixImage, Qt.AlignTop | Qt.AlignCenter)
        layout.addWidget(self.canvas, Qt.AlignTop | Qt.AlignCenter)
        self.centralWidget().setLayout(layout)
        print(f"{self.__class__.__name__}: info; initialized")

    def closeEvent(self, event: QCloseEvent):
        self.closed.emit()
        print(f"{self.__class__.__name__}: info; stopped")
        event.accept()

    def wheelEvent(self, event):
        if (event.angleDelta().y() > 0) and (self.image_scaling_factor > self.image_scaling_step):  # zooming in
            self.image_scaling_factor -= self.image_scaling_step
        elif (event.angleDelta().y() < 0) and (self.image_scaling_factor < 1.0):  # zooming out
            self.image_scaling_factor += self.image_scaling_step
        self.image_scaling_factor = round(self.image_scaling_factor, 2)  # strange behaviour, so rounding is necessary
        self.update()  # redraw the image with different scaling

    @pyqtSlot(np.ndarray)
    def update(self, image=None):
        self.kick_timer()  # Measure time delay
        if image is not None:  # we have a new image
            height, width = image.shape[:2]  # get dimensions
            self.image = image if self.image_size[0] == width and self.image_size[1] == height \
                else cv2.resize(image, self.image_size)
            if 0 < self.image_scaling_factor < 1:  # Crop the image to create a zooming effect
                height, width = image.shape[:2]  # get dimensions
                delta_height = round(height * (1 - self.image_scaling_factor) / 2)
                delta_width = round(width * (1 - self.image_scaling_factor) / 2)
                image = image[delta_height:height - delta_height, delta_width:width - delta_width]
            height, width = image.shape[:2]  # get dimensions
            if self.image_height != height or self.image_width != width:  # we need scaling
                scaling_factor = self.image_height / float(height)  # get scaling factor
                if self.image_width / float(width) < scaling_factor:
                    scaling_factor = self.image_width / float(width)
                    image = cv2.resize(image, None, fx=scaling_factor, fy=scaling_factor,
                                       interpolation=cv2.INTER_AREA)  # resize image
            if len(image.shape) < 3:  # check nr of channels
                image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)  # convert to color image
            height, width = image.shape[:2]  # get dimensions
            q_image = QImage(image.data, width, height, width * 3,
                             QImage.Format_RGB888)  # Convert from OpenCV to PixMap
            self.PixImage.setPixmap(QPixmap(q_image))
            self.PixImage.show()

    def kick_timer(self):
        clock_time = int(round(time() * 1000))
        if self.prev_clock_time is not None:
            time_diff = clock_time - self.prev_clock_time
            self.timerLabel.setNum(round(time_diff))
        self.prev_clock_time = clock_time

    @pyqtSlot()
    def disableButtons(self):
        self.homeButton.setEnabled(False)
        self.snapshotButton.setEnabled(False)
        self.recordButton.setEnabled(False)
        self.stageOriginButton.setEnabled(False)
        self.runButton.setEnabled(False)
        self.light.setEnabled(False)
        self.stageXTranslation.setEnabled(False)
        self.stageYTranslation.setEnabled(False)
        self.stageZTranslation.setEnabled(False)
        self.autoFocusButton.setEnabled(False)

    @pyqtSlot()
    def enableButtons(self):
        self.homeButton.setEnabled(True)
        self.snapshotButton.setEnabled(True)
        self.recordButton.setEnabled(True)
        self.runButton.setEnabled(True)
        self.stageOriginButton.setEnabled(True)
        self.light.setEnabled(True)
        self.stageXTranslation.setEnabled(True)
        self.stageYTranslation.setEnabled(True)
        self.stageZTranslation.setEnabled(True)
        self.autoFocusButton.setEnabled(True)

    @pyqtSlot(int)
    def updateProgressBar(self, val):
        if 0 <= val <= 100:
            self.progress_bar.setValue(val)
