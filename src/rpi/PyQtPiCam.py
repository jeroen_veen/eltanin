"""
Based on
 https://picamera.readthedocs.io/en/latest/
 https://picamera.readthedocs.io/en/release-1.13/api_camera.html
 https://www.raspberrypi.org/documentation/hardware/camera/

version December 2020, January 2021
Note that there are different image sizes involved:
1. capture_frame_size: image capture frame size, obtained from the sensor mode
2. video_frame_size: video capture frame size
3. display_frame_size: size of frames that get emitted to the processing chain.
"""
import numpy as np
from time import time
from cv2 import putText, FONT_HERSHEY_SIMPLEX
from os import path, sep, makedirs
from fps import FPS
from picamera import PiCamera
from picamera.array import PiRGBArray, PiArrayOutput
from PyQt5.QtCore import QThread, pyqtSlot, pyqtSignal, QSettings
from wait import wait_signal, wait_ms
from io import BytesIO
from subprocess import run, check_output


def raw_frame_size(frame_size, splitter=False):
    """
    Round a (width, height) tuple up to the nearest multiple of 32 horizontally
    and 16 vertically (as this is what the Pi's camera module does for
    unencoded output).
    """
    width, height = frame_size
    f_width = (width + 15) & ~15 if splitter else (width + 31) & ~31
    f_height = (height + 15) & ~15
    return f_width, f_height


def frame_size_from_sensor_mode(sensor_mode):
    if sensor_mode == 0:
        frame_size = (4056, 3040)
    elif sensor_mode == 1:
        frame_size = (1920, 1080)
    elif sensor_mode == 2 or sensor_mode == 3:
        frame_size = (3280, 2464)
    elif sensor_mode == 4:
        frame_size = (1640, 1232)
    elif sensor_mode == 5:
        frame_size = (1640, 922)
    elif sensor_mode == 6:
        frame_size = (1280, 720)
    elif sensor_mode == 7:
        frame_size = (640, 480)
    else:
        raise ValueError
    return frame_size


def frame_size_from_string(frame_size_str):
    (width, height) = frame_size_str.split('x')
    return int(width), int(height)


class PiYArray(PiArrayOutput):
    """
    Produces a 2-dimensional Y only array from a YUV capture.
    Does not seem faster than PiYUV array...
    """

    def __init__(self, camera, size=None):
        super(PiYArray, self).__init__(camera, size)
        self.f_width, self.f_height = raw_frame_size(self.size or self.camera.resolution)
        self.y_len = self.f_width * self.f_height

    def flush(self):
        super(PiYArray, self).flush()
        a = np.frombuffer(self.getvalue()[:self.y_len], dtype=np.uint8)
        self.array = a[:self.y_len].reshape((self.f_height, self.f_width))


class PiVideoStream(QThread):
    """
    Thread that produces frames for further processing as a PyQtSignal.
    Picamera is set-up according to sensormode and splitter_port 0 is used for capturing image data.
    A video stream is set-up, using picamera splitter port 1 and resized to frameSize.
    Splitter_port 2 is used for capturing video at videoFrameSize.
    """
    finished = pyqtSignal()
    postMessage = pyqtSignal(str)
    frame = pyqtSignal(np.ndarray)
    progress = pyqtSignal(int)
    captured = pyqtSignal()

    def __init__(self, settings: QSettings = None):
        super().__init__()
        # warnings.filterwarnings('default', category=DeprecationWarning)
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings

        self.storage_path = self.settings.value('temp_folder', None, type=str)
        self.monochrome = self.settings.value('camera/monochrome', False, type=bool)
        self.sensor_mode = self.settings.value('camera/sensor_mode', 0, type=int)
        self.display_frame_size = frame_size_from_string(self.settings.value('gui_frame_size'))
        self.capture_frame_size = frame_size_from_sensor_mode(self.sensor_mode)

        self.camera = PiCamera()
        # self.videoStream = BytesIO()
        self.image, self.raw_capture, self.capture_stream = None, None, None
        self.cropRect = [0] * 4
        self.fps = 0
        print(f"{self.__class__.__name__}: info; initialized")

    @pyqtSlot()
    def init_stream(self):

        if self.isRunning():
            self.requestInterruption()
            wait_signal(self.finished, 10000)
        # Set camera parameters
        self.camera.sensor_mode = self.sensor_mode
        self.camera.resolution = self.capture_frame_size
        self.camera.framerate = self.settings.value('camera/frame_rate', 1, type=int)
        self.camera.image_effect = self.settings.value('camera/effect')
        self.camera.iso = self.settings.value('camera/iso', 100, type=int)
        # self.camera.video_denoise = self.settings.value('camera/video_denoise', False, type=bool)
        self.camera.exposure_mode = self.settings.value('camera/exposure_mode', 'auto', type=str)
        self.camera.awb_mode = self.settings.value('camera/awb_mode', 'auto', type=str)
        self.camera.meter_mode = self.settings.value('camera/meter_mode', 'average', type=str)

        # Wait for the automatic gain control to settle
        wait_ms(2000)

        if self.settings.value('camera/fix_settings_after_camera_start', False, type=bool):
            # Now fix the values
            self.camera.shutter_speed = self.camera.exposure_speed
            self.camera.exposure_mode = 'off'
            g = self.camera.awb_gains
            self.camera.awb_mode = 'off'
            self.camera.awb_gains = g

        # Setup capture
        if self.monochrome:
            self.raw_capture = PiYArray(self.camera, size=self.display_frame_size)
            self.capture_stream = self.camera.capture_continuous(self.raw_capture, 'yuv', use_video_port=True,
                                                                 splitter_port=1, resize=self.display_frame_size)
        else:
            self.raw_capture = PiRGBArray(self.camera, size=self.display_frame_size)
            self.capture_stream = self.camera.capture_continuous(self.raw_capture, 'bgr', use_video_port=True,
                                                                 splitter_port=1, resize=self.display_frame_size)
        # init crop rectangle
        if self.cropRect[2] == 0:
            self.cropRect[2] = self.camera.resolution[1]
        if self.cropRect[3] == 0:
            self.cropRect[3] = self.camera.resolution[0]

        # start the thread
        self.start(QThread.HighPriority)
        print(f"{self.__class__.__name__}: info; video stream initialized with frame size = "
              f"{self.camera.resolution} and {1 if self.monochrome else 3} channels")

    @pyqtSlot()
    def run(self):
        try:
            self.fps = FPS().start()
            for f in self.capture_stream:
                if self.isInterruptionRequested():
                    break
                self.raw_capture.seek(0)
                img = f.array  # grab the frame from the stream
                self.frame.emit(img)  # cv2.resize(img, self.frameSize[:2]))
                self.fps.update()

        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        finally:
            self.fps.stop()
            # shape = (self.display_frame_size[1], self.display_frame_size[0])
            if not self.monochrome:
                shape = (self.display_frame_size[1],self.display_frame_size[0]) + (3,)
            img = np.zeros(shape=shape, dtype=np.uint8)
            putText(img, 'Camera suspended',
                    (int(self.display_frame_size[0] / 2) - 150, int(self.display_frame_size[1] / 2)),
                    FONT_HERSHEY_SIMPLEX, 1, 255, 1)
            for i in range(5):
                wait_ms(100)
                self.frame.emit(img)
            self.postMessage.emit(f"{self.__class__.__name__}: "
                                  f"info; finished, approx. processing speed: {self.fps.fps()} fps")
            self.finished.emit()

    def pause(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; pausing")
        try:
            if self.isRunning():
                self.requestInterruption()
                wait_signal(self.finished, 10000)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        # finally:
        #     print(f"{self.__class__.__name__}: info; stopped")
        #     # self.camera.close()
        #     self.quit()  # Note that thread quit is required, otherwise strange things happen.

    @pyqtSlot()
    def stop(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopping")
        try:
            if self.isRunning():
                self.requestInterruption()
                wait_signal(self.finished, 10000)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        finally:
            # self.camera.close()  # results in error: 'output port couldn't be disabled'?
            self.quit()  # Note that thread quit is required, otherwise strange things happen.
            print(f"{self.__class__.__name__}: info; stopped")

    @pyqtSlot(str)
    def take_image(self, filename_prefix=None):
        self.postMessage.emit(f"{self.__class__.__name__}: info; snapshot requested")

        if filename_prefix is not None:
            (head, tail) = path.split(filename_prefix)
            if not path.exists(head):
                makedirs(head)
            filename = sep.join([head, '{:016d}_'.format(round(time() * 1000)) + tail + '.png'])
        else:
            filename = '{:016d}'.format(round(time() * 1000)) + '.png'
            # open path
            if self.storage_path is not None:
                filename = sep.join([self.storage_path, filename])
        try:
            self.camera.capture(filename, use_video_port=False, format='png')
            # When use_video_port is False the camera’s image port is used.
            # This port is slow but produces better quality pictures.
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

        self.postMessage.emit(f"{self.__class__.__name__}: info; image written to {filename}")
        self.captured.emit()

    @pyqtSlot(str, int)
    def record_clip(self, filename_prefix=None, duration=10):
        """
        Note that The H264 codec has a restriction of 1920 pixels wide, a max of 1920 pixels high,
        and nominally a maximum of 8192 16x16 macroblocks per frame, as specified in H264 Level 4.0/4.1
        So, although the sensor can grab higher resolution, the codec cannot process it.
        Note that while it seems possible to change the sensormode, reverting to the original mode fails when capturing an image.
        In many cases, the intended framerate is not achieved. For that reason, ffprobe counts
        the total number of frames that were actually taken.
        Next, the h264 video file is boxed using MP4box.
        For some reason, changing the framerate with MP4Box did not work out.
        """
        if filename_prefix is not None:
            (head, tail) = path.split(filename_prefix)
            if not path.exists(head):
                makedirs(head)
            filename = sep.join([head, '{:016d}_{}s'.format(round(time() * 1000), round(duration)) + tail])
        else:
            filename = '{:016d}_{}s'.format(round(time() * 1000), round(duration))
            if self.storage_path is not None:
                filename = sep.join([self.storage_path, filename])

        self.postMessage.emit(f"{self.__class__.__name__}: info; starting recording for {duration} s")
        splitter_port = 2

        try:
            # self.pause()
            # set-up different stream??
            #
            # # GPU resizes frames, and compresses to h264 stream
            # # vs = PiVideoStream(iso=100, resolution=(1600, 1200), sensor_mode=4, framerate=30, video_denoise=True).start()
            # video_camera = PiCamera()
            # video_camera.exposure_mode = 'backlight'  # 'auto'
            # video_camera.awb_mode = 'flash'  # 'auto'
            # video_camera.meter_mode = 'backlit'  # 'average'
            # video_camera.sensor_mode = 2
            # # # video_camera.resolution = self.capture_frame_size
            # video_camera.framerate = self.settings.value('camera/frame_rate', 1, type=int)
            # video_camera.image_effect = self.settings.value('camera/effect')
            # video_camera.iso = self.settings.value('camera/iso', 100, type=int)
            # video_camera.video_denoise = self.settings.value('camera/video_denoise', False, type=bool)
            #
            # # Wait for the automatic gain control to settle
            # # wait_ms(3000)
            #
            # # # Now fix the values
            # # video_camera.shutter_speed = video_camera.exposure_speed
            # # video_camera.exposure_mode = 'off'
            # # g = video_camera.awb_gains
            # # video_camera.awb_mode = 'off'
            # # video_camera.awb_gains = g
            if self.capture_frame_size[0] <= 1920:
                self.camera.start_recording(filename + '.h264', format='h264', splitter_port=splitter_port,
                                            sps_timing=True)
            else:
                self.camera.start_recording(filename + '.h264', format='h264', splitter_port=splitter_port,
                                            resize=(1640, 1232), sps_timing=True)
            wait_ms(duration * 1000)
            self.camera.stop_recording(splitter_port=splitter_port)
            self.start()

            # Wrap an MP4 box around the video
            nr_of_frames = check_output(
                ["ffprobe", "-v", "error", "-count_frames", "-select_streams", "v:0", "-show_entries",
                 "stream=nb_read_frames", "-of", "default=nokey=1:noprint_wrappers=1", filename + '.h264'])
            real_fps = float(nr_of_frames) / duration
            self.postMessage.emit(f"{self.__class__.__name__}: info; "
                                  f"video clip captured with real framerate: {real_fps} fps")
            # run(["MP4Box", "-fps", str(self.frameRate), "-add", filename + '.h264:fps=' + str(real_fps), "-new", filename + '.mp4'])
            # run(["MP4Box", "-fps", str(self.camera.framerate), "-add", filename + ".h264", "-new",
            #      filename + "_{}fr.mp4".format(int(nr_of_frames))])
            # Not sure if we should adapt intended framerate to measured frame rate??
            run(["MP4Box", "-add", filename + ".h264", "-new",
                 filename + "_{}fr.mp4".format(int(nr_of_frames))])
            run(["rm", filename + '.h264'])
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

        self.postMessage.emit(f"{self.__class__.__name__}: info; video clip written to {filename}")
        self.captured.emit()

    @pyqtSlot(int)
    def set_crop_x1(self, val):
        if 0 <= val <= self.cropRect[3]:
            self.cropRect[1] = val
        else:
            raise ValueError('crop x1')

    @pyqtSlot(int)
    def set_crop_x2(self, val):
        if self.cropRect[1] < val < self.camera.resolution[1]:
            self.cropRect[3] = val
        else:
            raise ValueError('crop x2')

    @pyqtSlot(int)
    def set_crop_y1(self, val):
        if 0 <= val <= self.cropRect[2]:
            self.cropRect[0] = val
        else:
            raise ValueError('crop y1')

    @pyqtSlot(int)
    def set_crop_y2(self, val):
        if self.cropRect[0] < val < self.camera.resolution[0]:
            self.cropRect[2] = val
        else:
            raise ValueError('crop y2')
