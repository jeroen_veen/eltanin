"""@package docstring
"""
from PyQt5.QtCore import QTimer, QObject, pyqtSignal, pyqtSlot
from urllib.request import urlopen
from urllib.error import HTTPError, URLError
from subprocess import Popen, check_output, CalledProcessError, PIPE, STDOUT
from rpi.checkOS import is_raspberry_pi
from socket import timeout


class CheckInternet(QObject):
    postMessage = pyqtSignal(str)
    error = pyqtSignal()

    timer = QTimer()

    def __init__(self, interval=100):
        super().__init__()
        self.interval = 1000 * interval
        self.timer.timeout.connect(self.update)
        self.timer.start(self.interval)

    def update(self):
        try:
            for i in range(0, 5):
                urlopen('http://www.google.com', timeout=10)  # ping google
        except (HTTPError, URLError) as e:
            if hasattr(e, 'reason'):
                error_str = f"failed to reach server, reason: {e.reason}"
            elif hasattr(e, 'code'):
                error_str = f"failed request, code: {e.code}"
            else:
                error_str = "unknown error"
            self.error.emit()
            self.postMessage.emit(f"{self.__class__.__name__}: error; {error_str}")
        except timeout:
            self.error.emit()
            self.postMessage.emit(f"{self.__class__.__name__}: error; socket timed out")

    @pyqtSlot()
    def stop(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopping")
        if self.timer.isActive():
            self.timer.stop()


class CheckWiFi(QObject):
    timer = QTimer()
    postMessage = pyqtSignal(str)

    def __init__(self, interval=100):
        super().__init__()
        if not is_raspberry_pi():
            print("This function is for Linux!")
        else:
            self.interval = 1000 * interval
            self.timer.timeout.connect(self.update)
            self.timer.start(self.interval)

    def update(self):
        ps = Popen(['iwgetid'], stdout=PIPE, stderr=STDOUT)
        try:
            output = check_output(('grep', 'ESSID'), stdin=ps.stdout)
        except CalledProcessError:
            # grep did not match any lines
            self.postMessage.emit(f"{self.__class__.__name__}: error; No wireless networks connected. grep: {output}")
            print("No wireless networks connected")


if __name__ == "__main__":
    from PyQt5.QtCore import QCoreApplication
    from testMessageSink import TestMessageSink

    app = QCoreApplication([])

    ci = CheckInternet(20)
    tm = TestMessageSink()

    ci.postMessage.connect(tm.receive_message)

    app.exec_()
