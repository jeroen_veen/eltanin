"""
@package printhat.py
@brief printhat.py contains classes for stepper motor control, G-code string creation, homing and well positioning.
@author Gert van Lagen, Jeroen Veen
"""
import re
import io
import serial
from wait import wait_ms, wait_signal
from subprocess import Popen, check_output
from PyQt5.QtCore import QSettings, QThread, pyqtSignal, pyqtSlot

EPS = 1e-4

class PrintHat(QThread):
    postMessage = pyqtSignal(str)
    reply = pyqtSignal(str)
    homed = pyqtSignal()
    confirmed = pyqtSignal()
    finished = pyqtSignal()

    settings: QSettings
    port, ser, sio = None, None, None
    is_homed = False  # PrintHat motors have been homed
    is_ready = False  # Printhat and klipper are ready
    reply_msg = None

    def __init__(self, settings: QSettings = None):
        super().__init__()
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings
        self.port = self.settings.value('printhat/port')

        print(f"{self.__class__.__name__}: info; initialized")

    @pyqtSlot()
    def connect_klipper(self):
        try:
            self.postMessage.emit(f"{self.__class__.__name__}: info; starting Klipper service")
            Popen(['sudo', 'service', 'klipper', 'restart'])
            wait_ms(500)
            output = check_output(['sudo', 'service', 'klipper', 'status'])
            self.postMessage.emit(f"{self.__class__.__name__}: info; Klipper status:\n {output.decode('UTF-8')}")

            self.ser = serial.Serial(self.port, 250000, timeout=1)
            self.sio = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser), line_buffering=True, encoding="utf-8",
                                        errors='replace', newline="\r\n")

            if self.sio:
                self.postMessage.emit(f"{self.__class__.__name__}: info; Connected to printhat")
                self.is_ready = True
                self.restart()
                self.start(QThread.HighPriority)
            else:
                self.postMessage.emit(f"{self.__class__.__name__}: error; Cannot connect to printhat")

        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    def run(self):
        try:
            while True:
                if self.isInterruptionRequested():
                    break
                elif self.sio:
                    msg = self.sio.readline()
                    if msg:
                        self.postMessage.emit(f"{self.__class__.__name__}: info; printhat replied: {msg}")
                        self.reply.emit(msg)
                        self.reply_msg = msg
                        msg = msg.lower()
                        # protocol is not fully implemented,
                        #  see https://reprap.org/wiki/G-code#Replies_from_the_RepRap_machine_to_the_host_computer
                        if 'klipper state' in msg:
                            if any(x in msg for x in ['ready', 'ok']):
                                self.postMessage.emit(f"{self.__class__.__name__}: info; printhat ready")
                                self.is_ready = True
                            elif any(x in msg for x in ['shutdown', 'disconnect']):
                                self.postMessage.emit(f"{self.__class__.__name__}: error; printhat not ready")
                                self.is_ready = False
                        if all(x in msg for x in ['!!', 'must home axis']):
                            self.postMessage.emit(f"{self.__class__.__name__}: error; printhat not homed")
                            self.is_homed = False
                        if all(x in msg for x in ['!!', 'not ready']):
                            self.postMessage.emit(f"{self.__class__.__name__}: error; printhat not ready")
                            self.is_ready = False
                        # return reply_msg
                else:
                    self.postMessage.emit(f"{self.__class__.__name__}: error; Not connected to printhat")
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        finally:
            self.postMessage.emit(f"{self.__class__.__name__}: info; finished")
            self.finished.emit()

    # @brief PrintHat::send_gcode writes a byte array containing a G-code to the serial port.
    # @param gcode_string is the string to be written to the serial port.
    def send_gcode(self, gcode_string: str):
        if self.sio:
            try:
                self.sio.write(gcode_string + "\r\n")
                self.sio.flush()  # it is buffering. required to get the data out *now*
                self.postMessage.emit(f"{self.__class__.__name__}: info; send gcode: {gcode_string}")
                # return await self.reply()
                # wait_signal(self.reply)
            except Exception as err:
                self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        else:
            self.postMessage.emit(f"{self.__class__.__name__}: error; Not connected to printhat")
        # return None # is this required?

    @pyqtSlot()
    def disconnect_klipper(self):
        try:
            self.postMessage.emit(f"{self.__class__.__name__}: info; stopping Klipper service")
            if self.ser is not None:
                self.ser.close()
            Popen(['sudo', 'service', 'klipper', 'stop'])
            output = check_output(['sudo', 'service', 'klipper', 'status'])
            self.postMessage.emit(f"{self.__class__.__name__}: info; Klipper status:\n {output.decode('UTF-8')}")
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def restart(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; restarting Klipper service")
        self.send_gcode("RESTART")
        self.send_gcode("M115")  # get firmware details
        wait_signal(self.reply, 30000)

    @pyqtSlot()
    def firmware_restart(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; restarting Klipper firmware")
        self.send_gcode("FIRMWARE_RESTART")

    @pyqtSlot()
    def go_home(self):
        if not self.is_ready:
            self.restart()
        if self.is_homed:
            # speed close to home
            self.goto_location(3 * [5], False)
        self.send_gcode("G28 X Y Z")  # move to origin
        wait_signal(self.reply, 30000)
        self.send_gcode("M400")  # Wait for current moves to finish
        wait_signal(self.reply, 30000)
        self.send_gcode("M114")  # get current location
        wait_signal(self.reply, 30000)
        self.postMessage.emit(f"{self.__class__.__name__}: info; home")
        self.is_homed = True
        
    @pyqtSlot()
    def go_to_load_location(self):
        x = self.settings.value('stage/load_location_offset_x_in_mm', 0, float)
        y = self.settings.value('stage/load_location_offset_y_in_mm', 0, float)
        self.goto_location(location = (0,200,None), relative = False)
        

    @pyqtSlot()
    def get_location(self) -> list:
        """ Get current location of the stage.
            :return: list of x,y,z
        """
        location = None
        self.send_gcode("M400")  # Wait for current moves to finish
        self.send_gcode("M114")  # get current location
        wait_signal(self.reply, 100000)
        reply_msg = self.reply_msg

        if reply_msg is not None and 'X:' in reply_msg:
            r = re.findall(r"[-+]?\d*\.\d+|\d+", reply_msg)
            location = [float(r[0]), float(r[1]), float(r[2])]
            self.postMessage.emit(f"{self.__class__.__name__}: info; current location = "
                                  f"{location[0]}, {location[1]}, {location[2]}")
        else:
            self.postMessage.emit(f"{self.__class__.__name__}: error; cannot obtain location")

        return location

    @pyqtSlot()
    def stop(self):
        """ Stop all motors, fan and illumination and shuts down the printHat.
            :return: None
        """
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopping")
        try:
            self.set_light_PWM(0.0)
            self.set_fan_PWM(0.0)
            self.goto_location(3 * [0], False)
            wait_signal(self.reply, 30000)
            if self.isRunning():
                self.requestInterruption()
                wait_signal(self.finished, 10000)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        finally:
            self.is_ready = False
            self.sio = None
            self.disconnect_klipper()
            self.quit()  # Note that thread quit is required, otherwise strange things happen.
            print(f"{self.__class__.__name__}: info; stopped")

    @pyqtSlot()
    def emergency_break(self):
        """ Stop all motors and shuts down the printHat. A firmware restart command is necessary to restart the system.
            :return: None
        """
        self.postMessage.emit(f"{self.__class__.__name__}: info; emergency break! restart the firmware")
        self.send_gcode("M112")

    @pyqtSlot()
    def firmware_restart(self):
        """ Restart the firmware and reload the config in the klipper software.
            :return: None
        """
        self.send_gcode("FIRMWARE_RESTART")

    @pyqtSlot(float)
    def set_light_PWM(self, val: float):
        """ Set PrintHAT light output pin to PWM value.
        Args:
            val (float): PWM dutycycle, between 0.0 and 1.0.
        Raises:
        :return: None
        """
        self.send_gcode("SET_PIN PIN=light VALUE=" + str(val))

    @pyqtSlot(float)
    def set_fan_PWM(self, val: float):
        """ Set PrintHAT fan output pin to PWM value.
            :param val: PWM dutycycle, float between 0.0 and 1.0.
        """
        clip_val = 1.0
        val = val if val < clip_val else clip_val
        self.send_gcode("SET_PIN PIN=rpi_fan VALUE={:1.2f}".format(val))

    @pyqtSlot(list, bool)
    def goto_location(self, location: list = None, relative: bool = True) -> list:
        """ Set PrintHAT fan output pin to PWM value.
            :rtype: object
            :param location: [x,y,z], note that if a parameter is None, it is not changed
            :param relative: wrt stage offset as defined in settings
            :return: list of x,y,z
        """
        if location is not None and len(location) == 3:
            x, y, z = tuple(location)

            if not self.is_homed:
                self.go_home()
            if x is not None and relative:
                x += self.settings.value('stage/offset_x_in_mm', 0, float)
            if y is not None and relative:
                y += self.settings.value('stage/offset_y_in_mm', 0, float)
            gcode_string = "G1"
            gcode_string += " X{:.3f}".format(x) if x is not None else ""
            gcode_string += " Y{:.3f}".format(y) if y is not None else ""
            gcode_string += " Z{:.3f}".format(z) if z is not None else ""
            self.send_gcode(gcode_string)  # move to location
            wait_signal(self.reply, 100000)
            reply_msg = self.reply_msg
            if reply_msg is not None and 'ok' in reply_msg:
                # check if we actually reached the correct location
                location_act = self.get_location()
                if location_act is not None:
                    x_act, y_act, z_act = location_act
                    if (x is None or abs(x-x_act) < EPS) \
                            and (y is None or abs(y - y_act) < EPS) \
                            and (z is None or abs(z - z_act) < EPS):
                        self.confirmed.emit()
                        return x_act, y_act, z_act
        return None
