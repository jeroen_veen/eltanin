from PyQt5.QtCore import QObject, pyqtSlot
from datetime import datetime


class TestMessageSink(QObject):
    def __init__(self):
        super().__init__()

    @pyqtSlot(str)
    def receive_message(self, s: str):
        s = datetime.now().isoformat() + " " + s
        print(s)
